﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Oracle.ManagedDataAccess.Client;

namespace DatabaseAdapter
{
    public class OracleData : IDatabase
    {
        private string host, port, username, password, SID;
        private bool connected;
        public bool Connected { get { return this.connected; } }
        public string ConnectionString
        {
            get
            {
                return "Data Source=" + this.host + ":" + this.port + ";User Id=" + this.username + ";Password=" + this.password + ";";
            }
        }

        private OracleConnection connection;

        public OracleData(string host, string port, string username, string password, string SID)
        {
            this.host = host;
            this.port = port;
            this.username = username;
            this.password = password;
            this.SID = SID;
            this.connected = false;
        }

        public bool Connect()
        {
            this.connection = new OracleConnection(this.ConnectionString);
            this.connection.Open();
            this.connected = true;
            return this.connected;
        }

        public void Disconnect()
        {
            this.connection.Close();
            this.connected = false;
        }

        public DataSet ExecuteSqlQuery(string query)
        {
            DataSet result;

            using (var resultTable = new OracleDataAdapter(query, this.connection))
            {
                result = new DataSet();
                resultTable.Fill(result);
            }
            return result;
        }

        public bool TestConnection()
        {
            throw new NotImplementedException();
        }
    }
}
