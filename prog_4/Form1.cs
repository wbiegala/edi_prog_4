﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace prog_4
{
    public partial class Form1 : Form
    {
        List<int> identyfikatory;
        DataSet result;

        public Form1()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                identyfikatory = new List<int>();
                string[] ids = textBox2.Text.Split(';');
                foreach (var item in ids)
                {
                    if (Int32.TryParse(item, out int result))
                    {
                        identyfikatory.Add(result);
                    }
                }
                
                string inQueryPart = "";

                foreach (var item in identyfikatory)
                {
                    inQueryPart += "'" + item +"', ";
                }
                inQueryPart = inQueryPart.Remove(inQueryPart.Length - 2);

                DatabaseAdapter.IDatabase connection = new DatabaseAdapter.OracleData(hostTextBox.Text, portTextBox.Text, usernameTextBox.Text, passwordTextBox.Text, sidTextBox.Text);
                connection.Connect();
                string temp = "SELECT * FROM \"" + this.tableTextBox.Text + "\" WHERE ID in (" + inQueryPart + ")";
                result = connection.ExecuteSqlQuery(temp);
                dataGridView1.DataSource = null;
                dataGridView1.AutoGenerateColumns = true;
                dataGridView1.DataSource = result.Tables[0];

                connection.Disconnect();


                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Błąd!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

    }
}
